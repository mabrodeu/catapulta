# catapulta
## Par Pier Olivier Bourgault et [Marc-Antoine Brodeur](https://github.com/mabrodeur)

### Concept
Catapulte autonome qui scanne l'espace devant elle et quand elle détecte une personne ou un objet, elle lui lance une balle dessus.

##Pour lancer le projet, il faut :
1. Brancher les 2 câbles de courant dans une prise 5V.
2. Libérer l'espace devant la catapulte pour qu'elle s'initialise.
3. Mettre une balle dans la coupe.
4. Amusez-vous!

### Liste de Matériel

-	Arduino Leonardo (1)
-	Condensateur 220μF (1)
-	Servomoteur 180° (3)
-	Capteur infrarouge (1)
-	Adaptateur secteur 5V (2)
-	Câbles de courant USB (2) 
-	Tige de bois (3x 1 pied)
-	Cuillères à mesurer (1, pour la coupe qui tien la balle)
-	Élastique (2)
-	Longue vis (1)
-	Boulons ou autre pour remplir l'espace entre les tiges de bois
- 	Vis pour tenir le tout
-	Câblage
-	Colle chaude

#### Schéma de branchement
![Schéma](https://raw.githubusercontent.com/mabrodeur/catapulta/master/images/schema.png)

#### Montage complété 
![Démo](https://raw.githubusercontent.com/mabrodeur/catapulta/master/images/catapulte.gif)
![Montage complété](https://raw.githubusercontent.com/mabrodeur/catapulta/master/images/montage_final.JPG)



### License
Voir le fichier [LICENSE](https://raw.githubusercontent.com/mabrodeur/jardin-interactif/master/LICENSE)
