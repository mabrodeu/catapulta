#include <Servo.h>

// Smoothing
const int numReadings = 10;
int readings[numReadings];      // the readings from the analog input
int index = 0;                  // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average

int facteurRotation = 1;

Servo myservo;
int angle = 500;
Servo launchServo;
int launch = 2000;
Servo scanServo;
int scan = 0;

#define CAPTEUR_PIN 0

// State 0 = scanning
// State 1 = launch
// State 2 = recharge
int state = 2;

void setup()
{
  Serial.begin(57600);

  myservo.attach(9);
  myservo.writeMicroseconds(500);
  launchServo.attach(10);
  launchServo.writeMicroseconds(1100);
  
  scanServo.attach(11);
  scanServo.writeMicroseconds(0);

  initAverage();  
}

void initAverage(){
  for (int thisReading = 0; thisReading < numReadings; thisReading++)
    readings[thisReading] = 0;
    
  average = 0;
  total = 0;
  
  delay(5000);
}

void evaluateAverage(){
  // See : http://arduino.cc/en/Tutorial/Smoothing
  // subtract the last reading:
  total = total - readings[index];         
  // read from the sensor:  
  readings[index] = analogRead(CAPTEUR_PIN); 
  // add the reading to the total:
  total= total + readings[index];       
  // advance to the next position in the array:  
  index = index + 1;                    
  
  // if we're at the end of the array...
  if (index >= numReadings)              
  // ...wrap around to the beginning: 
    index = 0;                           
  
  // calculate the average:
  average = total / numReadings;         
  // send it to the computer as ASCII digits
  //Serial.println(average);   
  
  delay(1);        // delay in between reads for stability 
}

void loop() {
  
  evaluateAverage();
  
  // scanning
  if(state == 0){
    
    scanServo.writeMicroseconds(scan);
    delay(1);
    scan += facteurRotation;
    if ( scan > 2000 ) {
      facteurRotation = -1;
    } else if (scan < 1000) {
      facteurRotation = 1;
    }
      
    if(average > 100){
       //Serial.println("state switch to 1");
       state = 1;
     }  
  
  }
  
  // launch
  else if(state == 1){
    //Serial.println("Launch! state switch to 2");
    launchServo.writeMicroseconds(1100);
    state = 2;
  }
  
  // recharge
  else if(state == 2){
    //Serial.println("Recharge! state switch to 0");
    delay(1500);
    myservo.writeMicroseconds(500);
    delay(1500);
    launchServo.writeMicroseconds(2000);
    delay(1000);
    myservo.writeMicroseconds(2300);
    delay(500);
    
    initAverage();
    state = 0;
  }

}
